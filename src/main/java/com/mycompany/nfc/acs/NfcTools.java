/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.nfc.acs;

import com.sun.jndi.toolkit.url.Uri;
import com.sun.org.apache.xml.internal.utils.URI;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.nfctools.*;
import org.nfctools.ndef.Record;
import javax.smartcardio.Card;
import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.TerminalFactory;
import org.nfctools.io.NfcDevice;
import org.nfctools.utils.NfcUtils;

/**
 *
 * @author kevinalejandrocastano
 */
public class NfcTools {

    public static void main(String[] args) throws CardException, UnsupportedEncodingException, MalformedURLException, IOException, URISyntaxException {

        TerminalFactory factory = TerminalFactory.getDefault();
        List<CardTerminal> terminals = factory.terminals().list();
        CardTerminal terminal = terminals.get(0);
        if (terminal.isCardPresent()) {
            System.out.println("card present/istalled");
        }

        if (terminal.waitForCardPresent(5000)) {
            Card card;
            System.out.println(terminal.getName());
            try {

                card = terminal.connect("*");
                //System.out.println(card.getATR().getBytes().length);
                for (byte historicalByte : card.getATR().getHistoricalBytes()) {
                    System.out.print(historicalByte);
                }
                System.out.println("");

                System.out.println(NfcUtils.convertBinToASCII(card.getATR().getBytes()));

                String gg = NfcUtils.convertBinToASCII(card.getATR().getHistoricalBytes());
                System.out.println(new String(card.getATR().getHistoricalBytes(), "UTF-8"));
                System.out.println(gg);
               
                String uri = "https://smartcard-atr.appspot.com/parse?ATR="+NfcUtils.convertBinToASCII(card.getATR().getBytes());
                java.net.URI urii = new java.net.URI(uri);
                java.awt.Desktop.getDesktop().browse(urii);
            } catch (CardException e) {
                System.out.println(e.fillInStackTrace());
            } catch (URI.MalformedURIException ex) {
                Logger.getLogger(NfcTools.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

}
